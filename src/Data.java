import java.time.LocalTime;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class Data {
   private final HashMap<String, Location> locationMap = new HashMap<>();
   private final SortedMap<String, Route> routeMap = new TreeMap<>();

   public Data() {
      /// === Locations A ... F ========

      var location = new Location("A");
      locationMap.put(location.getName(), location);

      location = new Location("B");
      locationMap.put(location.getName(), location);

      location = new Location("C");
      locationMap.put(location.getName(), location);

      location = new Location("D");
      locationMap.put(location.getName(), location);

      location = new Location("E");
      locationMap.put(location.getName(), location);

      location = new Location("F");
      locationMap.put(location.getName(), location);

      ////////////////////////////////////////////////////////////

      /// === Routes A-B-C-D ========
      for (int hour = 7; hour <= 19; hour += 2) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("A"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
         route.addEndPoint(locationMap.get("D"), LocalTime.of(hour, 46));
         routeMap.put(route.getKey(), route);
         route.write();
      }

      /// === Routes D-C-B-A
      for (int hour = 7; hour <= 19; hour += 2) {
         var departure = LocalTime.of(hour, 0);
         var route = new Route(locationMap.get("D"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
         route.addEndPoint(locationMap.get("A"), LocalTime.of(hour, 46));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes E-B-C-F ========
      for (int hour = 7; hour <= 19; hour += 1) {
         var departure = LocalTime.of(hour, 30);
         var route = new Route(locationMap.get("E"), departure);
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
         route.addEndPoint(locationMap.get("F"), LocalTime.of(hour, 46));
         routeMap.put(route.getKey(), route);
      }

      /// === Routes F-C-B-E ========
      for (int hour = 7; hour <= 19; hour += 1) {
         var departure = LocalTime.of(hour, 30);
         var route = new Route(locationMap.get("F"), departure);
         route.addStopOver(locationMap.get("C"), LocalTime.of(hour, 15), LocalTime.of(hour, 16));
         route.addStopOver(locationMap.get("B"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
         route.addEndPoint(locationMap.get("E"), LocalTime.of(hour, 46));
         routeMap.put(route.getKey(), route);
      }

      // === Route B-C ===
      var departureBC = LocalTime.of(12, 0);
      var routeBC = new Route(locationMap.get("B"),departureBC);
      routeBC.addEndPoint(locationMap.get("C"), LocalTime.of(departureBC.getHour(), 15));
      routeMap.put(routeBC.getKey(), routeBC);


      // === Route C-B ===
      var departureCB = LocalTime.of(12, 0);
      var routeCB = new Route(locationMap.get("B"),departureCB);
      routeCB.addEndPoint(locationMap.get("C"), LocalTime.of(departureCB.getHour(), 15));
      routeMap.put(routeCB.getKey(), routeCB);
   }

   public void writeAllRoutes() {
      int count = 0;
      for (var route : routeMap.values()) {
         System.out.format("%2d. ", ++count);
         route.write();
      }
   }

   public void writeRoutesABCD() {
      int count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var route = e.getValue();
         if (key.contains("A-B-C-D")) {
            System.out.format("%2d. ", ++count);
            route.write();
         }
      }
   }

   public void writesRoutesBD() {
      int count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var route = e.getValue();

         var posB = key.indexOf("B");
         var posD = key.indexOf("D");

         if (posB < posD) {
            System.out.format("%2d. ", ++count);
            route.write();
         }
      }
   }

   public void writeRoutes(String key1, String key2) {
      int count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var route = e.getValue();

         var posB = key.indexOf(key1);
         var posD = key.indexOf(key2);

         if (posB < posD) {
            System.out.format("%2d. ", ++count);
            route.write();
         }
      }
   }

   public void writeRoutes(String key1, String key2, LocalTime t) {
      int count = 0;
      for (var e : routeMap.entrySet()) {
         var key = e.getKey();
         var route = e.getValue();

         var posB = key.indexOf(key1);
         var posD = key.indexOf(key2);

         if (posB < posD) {
            var halte = route.getStopOver(key1);
            var vertrek = halte.getDeparture();

            if (vertrek.isAfter(t)){
               System.out.format("%2d. ", ++count);
               route.write();
            }
         }
      }
   }

}